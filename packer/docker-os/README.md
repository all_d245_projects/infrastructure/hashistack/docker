# Docker-OS

The docker-os builder creates a docker image using the packer [docker builder](https://developer.hashicorp.com/packer/integrations/hashicorp/docker/latest/components/builder/docker) to support OS builds. Provisioning is handled by shell scripts and ansible.

**<u>Configurations</u>**  
The docker image uses the official [python](https://hub.docker.com/_/python) image as a base image with the following configurations:

- `ENTRYPOINT`: An entrypoint script to override the default behavior of the base packer image.
- `ENV PACKER_PLUGIN_PATH`: Path to the directory where packer stores downloaded plugins.
- `ENV PACKER_CACHE_DIRE`: Path to the directory where packer stores its cahce.

The packer image is provisioned using the ansible provisioner with the following installing steps:
1. Install required packages.
2. Create required directories.
3. Install Packer
4. Install 1password cli
5. install gitlab-release cli

Configuration variables can be found in the file `packer/docker/variables.pkr.hcl`.

**<u>Building The Image</u>**  
All commands should be executed from the root directory.

1. Install all required dependencies locally:
   - [packer](https://developer.hashicorp.com/packer/install?product_intent=packer)

2. Install the required packer plugins for the docker-os builder:
   ```bash
   PACKER_CACHE_DIR=files/packer/cache/.packer_cache \
   PACKER_PLUGIN_PATH=files/packer/cache/.packer_plugins \
   packer init -only='docker-os.*' packer/docker-os
   ```

3. Validate the configurations:
   ```bash
   PACKER_CACHE_DIR=files/packer/cache/.packer_cache \
   PACKER_PLUGIN_PATH=files/packer/cache/.packer_plugins \
   packer validate -only='docker-os.*' packer/docker-os
   ```

4. Build the image:
   a. prod (The prod build pushes to the registry): 
   ```bash
   PACKER_CACHE_DIR=files/packer/cache/.packer_cache \
   PACKER_PLUGIN_PATH=files/packer/cache/.packer_plugins \
   packer build -only=docker-os.docker.prod packer/docker-os
   ```

   b. local (The local build does not push to the registry): 
   ```bash
   PACKER_CACHE_DIR=files/packer/cache/.packer_cache \
   PACKER_PLUGIN_PATH=files/packer/cache/.packer_plugins \
   packer build -only=docker-os.docker.local packer/docker-os
   ```

**<u>Running The Image</u>**  
After the image has been succesfully built, it can be run by passing commands directly to the container eg (using local build): `docker run --rm hashistack/docker-os:local packer -version`

**<u>Pushing The Image To The Registry</u>**  
By default, the build will push the image to the cloud container registry using the default tags during prod builds if the docker push variables have not been overrided.

During local builds, its strongly recommended to modify the following variables:
- `docker_tag_registry=hashistack/docker-os`
- `docker_tag_tags=["local"]`
