# HashiStack - Docker

This repository holds configurations to build, tag, and deploy custom docker images using the [packer docker plugin](https://developer.hashicorp.com/packer/integrations/hashicorp/docker) to support operations for the HashiStack project.

## Directory Structure
`packer/`: The packer directory holds the packer configurations responsible for building, tagging and deploying custom images.

`ansible/`: The ansible directory holds all ansible related files for the provisioning of each docker build.

`files/`: The files directory holds general purpose files which packer uses during it's builds.

## Packer Builds
There are multiple packer builds each serviing a specific docker requirement:

1. **[docker-pipeline](packer/docker-pipeline/README.md):** The docker build creates a docker image with required tools for use in a CI/CD pipeline.
2. **[docker-os](packer/docker-os/README.md):** The os build creates a docker image to support building OS images.
3. **[docker-consul](packer/docker-consul/README.md):** The consul build creates a docker image to support provisioning consul clusters.
4. **[docker-nomad](packer/docker-nomad/README.md):** The nomad build creates a docker image to support provisioning nomad clusters.
5. **[docker-vault](packer/docker-vault/README.md):** The vault build creates a docker image to support provisioning vault clusters.


## Git Commit
The project adheres to the commit message [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#summary)
The structure used is
```
<type>: <issue title> [<issue-code>]
Closes <issue code>
```

An example commit would be:
```
feat: implement docker tagging [#21]
Closes #21
```

The supported `types` for this project are defined in `commitlint.config.js`.

This commit structure is enforced as part of the pre-commit hook configured via husky, as well as in the pipelines.

## Husky (git hooks)
Husky is used to register some actions during some git hooks. At the moment, the following git hooks are registered:
- commit-msg: Validate commit messages against `commitlint.config.js`

## Pipeline
Every merge request to the branch `dev` and `main` will trigger a detached pipeline.
The pipeline has the following jobs:

- Merge Request
   - install the npm dependencies.
   - Lint the commit message.
   - imports and runs pipeline jobs for each packer build if changes are detected
  
- Dev branch
  - imports and runs pipeline jobs for each packer build if changes are detected
